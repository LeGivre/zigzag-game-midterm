﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComtroler : MonoBehaviour
{
    public Transform target;

    private Vector3 offset;
    void Start()
    {
        offset = transform.position - target.transform.position;
    }

    void Update()
    {
        if (target.gameObject.GetComponent<PlayerController>().canMove)
            transform.position = target.transform.position + offset;
    }
}
