﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{

    private Rigidbody rb;
    private bool isMovingRight = false;
    private int count;

    public bool canMove = true;
    public float speed = 4f;
    public Text countText;
	public GameObject winText;


    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            isMovingRight = !isMovingRight;
            ChangeDirection();
        }

        if (Physics.Raycast(this.transform.position, Vector3.down * 2) == false)
        {
            canMove = false;
            rb.velocity = new Vector3(0f, -speed, 0f);
			winText.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("HomeMenu");
        }

    }

    private void ChangeDirection()
    {
        if (isMovingRight)
            rb.velocity = new Vector3(speed, 0f, 0f);
        else
            rb.velocity = new Vector3(0f, 0f, speed);

    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
			setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Score : " + count.ToString();
    }

}
