﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private float size;
    private Vector3 lastPos;

    public GameObject platform;
    public GameObject pickUp;

    void Start()
    {
        size = platform.transform.localScale.x;
        lastPos = platform.transform.position;
        for (var i = 0; i < 24; i++)
        {
            SpawnZ();
        }
        InvokeRepeating("SpawnPlatform", 2f, 0.2f);
    }

    private void SpawnPlatform()
    {
        int random = Random.Range(0, 6);
        int pickUpRand = Random.Range(0, 7);

        if (random >= 3)
            SpawnZ();
        else
            SpawnX();

        if (pickUpRand < 2)
            SpawnPickUp();
    }

    private void SpawnPickUp()
    {
        Instantiate(pickUp, lastPos + new Vector3(0f, 0.7f, 0f), pickUp.transform.rotation);
    }

    private void SpawnX()
    {
        GameObject _platform = Instantiate(platform) as GameObject;
        _platform.transform.position = lastPos + new Vector3(size, 0f, 0f);
        lastPos = _platform.transform.position;
    }

    private void SpawnZ()
    {
        GameObject _platform = Instantiate(platform) as GameObject;
        _platform.transform.position = lastPos + new Vector3(0f, 0f, size);
        lastPos = _platform.transform.position;
    }
}
